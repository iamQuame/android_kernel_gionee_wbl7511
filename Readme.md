G I O N E E  S+
Kernel 3.18.x

Building the Kernel
==================

<u><b>$  sudo -s</b></u>
<u><b>$ . build.sh</b></u>


Contribuitors
-------------
[*Jai Sharma*](https://github.com/jsharma44)
[*Iacob Ionut*](https://github.com/DarkWoodens)
